/**
 * Created by mislam on 3/15/2016.
 */

function showSubmit()
{
$("#submit").addClass('active');
    $("#submission").removeClass('active');
    $("#createSubmission").show();
    $("#viewSubmission").hide();
}
function viewSubmission()
{
    $("#submit").removeClass('active');
    $("#submission").addClass('active');
    $("#createSubmission").hide();
    $("#viewSubmission").removeClass('hidden');
    $("#viewSubmission").show();
}
function showReviewed()
{
    $("#reviewed").show();
    $("#revd").addClass('active');
    $("#revg").removeClass('active');
    $("#urev").removeClass('active');
    $("#inreview").hide();
    $("#unassigned").hide();
}

function showReviewing()
{
    $("#inreview").removeClass('hidden');
    $("#inreview").show();
    $("#revg").addClass('active');
    $("#reviewed").hide();
    $("#unassigned").hide();
    $('#crtadmin').hide();
    $('#crtreviewer').hide();
    $("#revd").removeClass('active');
    $("#urev").removeClass('active');
    $("#cadm").removeClass('active');
    $('#crvr').removeClass('active');
}
function showUnassigned()
{
    $("#unassigned").removeClass('hidden');
    $("#unassigned").show();
    $("#revd").removeClass('active');
    $("#revg").removeClass('active');
    $("#cadm").removeClass('active');
    $('#crvr').removeClass('active');
    $("#urev").addClass('active');
    $("#reviewed").hide();
    $("#inreview").hide();
    $('#crtadmin').hide();
    $('#crtreviewer').hide();
}
function showCrtAdmin()
{
    $('#crtadmin').removeClass('hidden');
    $('#crtadmin').show();
    $('#cadm').addClass('active');
    $("#revd").removeClass('active');
    $("#revg").removeClass('active');
    $("#urev").removeClass('active');
    $('#crvr').removeClass('active');
    $("#reviewed").hide();
    $("#unassigned").hide();
    $("#inreview").hide();
    $('#crtreviewer').hide();
}
function showCrtReviewer()
{
    $("#crtreviewer").removeClass('hidden');
    $('#crtreviewer').show();
    $('#crvr').addClass('active');
    $("#revd").removeClass('active');
    $("#revg").removeClass('active');
    $("#urev").removeClass('active');
    $('#cadm').removeClass('active');
    $("#reviewed").hide();
    $("#unassigned").hide();
    $("#inreview").hide();
    $('#crtadmin').hide();
}